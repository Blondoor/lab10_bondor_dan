package service;

import domain.Assign;

import java.io.IOException;

public interface AssignService extends Service<Long, Assign> {
    void addAssignment(Assign assign) throws IOException;
    long getMostAssignedProblem();
    void deleteAssignment(Long id);

}
