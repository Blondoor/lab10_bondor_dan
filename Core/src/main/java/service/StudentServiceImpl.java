package service;



import domain.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repository.StudentRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Component("studentService")
public class StudentServiceImpl implements StudentService {
    public static final Logger log = LoggerFactory.getLogger(StudentServiceImpl.class);

    @Autowired
    private StudentRepository studentRepo;

    @Override
    public void addStudent(Student student) {
        log.trace("addStudent - method entered");
        studentRepo.save(student);
        log.trace("addStudent - method finished");
    }

    @Override
    public Set<Student> getAll()
    {       log.trace("getAll - method entered");
            Iterable<Student> Student = studentRepo.findAll();
            log.trace("getAll - method finished");
            return StreamSupport.stream(Student.spliterator(), false).collect(Collectors.toSet());
    }

    @Override
    public Set<Student> filterStudentsByName(String n)
    {
        log.trace("filterStudentsByName - method entered");
        Iterable<Student> students = studentRepo.findAll();

        Set<Student> filtered=new HashSet<>();
        students.forEach(filtered::add);
        filtered.removeIf(stu->!stu.getName().contains(n));
        log.trace("filterStudentsByName - method finished");
        return filtered;
    }

    @Override
    public void deleteStudent(Long id)
    {
        log.trace("deleteStudent - method entered");
       studentRepo.deleteById(id);
        log.trace("deleteStudent - method finished");
    }

    @Override
    @Transactional
    public void updateStudent(Student student)
    {
        log.trace("updateStudent - method entered");
        studentRepo.findById(student.getId()).ifPresent(s -> {
            s.setSerialNumber(student.getSerialNumber());
            s.setName(student.getName());
            s.setSgroup(student.getSgroup());
        });
        log.trace("updateStudent - method finished");
    }

    @Override
    public Student getByID(Long id)
    {
        log.trace("getByID - method entered");
        Optional<Student> student = studentRepo.findById(id);
        log.trace("getByID - method finished");
        if(student.isPresent())
            return student.get();
        else throw new RuntimeException("Couldn't find the student with the given id");
    }
}
