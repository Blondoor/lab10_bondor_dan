package service;

import domain.Assign;
import repository.AssignRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component("assignService")
public class AssignServiceImpl implements AssignService {
    public static final Logger log = LoggerFactory.getLogger(AssignServiceImpl.class);

    @Autowired
    private AssignRepository assignRepo;

    @Override
    public Set<Assign> getAll()
    {
        log.trace("getAll - method entered");
        Iterable<Assign> assigns = assignRepo.findAll();
        log.trace("getAll - method finished");
        return StreamSupport.stream(assigns.spliterator(), false).collect(Collectors.toSet());
    }

    @Override
    public Assign getByID(Long ID)
    {
        log.trace("getByID - method entered");
        Optional<Assign> assign = assignRepo.findById(ID);
        log.trace("getByID - method finished");
        if(assign.isPresent())
            return assign.get();
        else throw new RuntimeException("Couldn't find an assignment with the given id");
    }

    @Override
    public void addAssignment(Assign assign)
    {
        log.trace("addAssignment - method entered");
        assignRepo.save(assign);
        log.trace("addAssignment - method finished");
    }

    @Override
    public void deleteAssignment(Long id)
    {
        log.trace("deleteAssignment - method entered");
        assignRepo.deleteById(id);
        log.trace("deleteAssignment - method finished");
    }

    @Override
    public long getMostAssignedProblem()
    {
        log.trace("getMostAssignedProblem - method entered");
        Iterable<Assign> assigns = assignRepo.findAll();
        log.trace("getMostAssignedProblem - method finished");
        List<Long> listOfAssigns = new ArrayList<Long>();
        assigns.forEach(x -> listOfAssigns.add(x.getProblemsid()));
        listOfAssigns.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream().max(Comparator.comparing(Map.Entry::getValue)).map(Map.Entry::getKey).orElse((long) - 1);
        return listOfAssigns.get(0);
    }
}
