package service;

import domain.Student;

import java.io.IOException;
import java.util.Set;

public interface StudentService extends Service<Long, Student> {
    void addStudent(Student student) throws IOException;
    Set<Student> filterStudentsByName(String s);
    void deleteStudent(Long id);
    void updateStudent(Student s);
}
