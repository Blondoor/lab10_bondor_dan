package service;

import domain.BaseEntity;

import java.io.Serializable;
import java.util.Set;

public interface Service<ID extends Serializable, T extends BaseEntity<ID>> {
    Set<T> getAll();
    T getByID(ID id);
}
