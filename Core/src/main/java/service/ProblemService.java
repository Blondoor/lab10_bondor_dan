package service;

import domain.Problem;

import java.io.IOException;
import java.util.Set;

public interface ProblemService extends Service<Long, Problem> {
    void addProblem(Problem p) throws  IOException;
    Set<Problem> filterProblemsByName(String s);
    void deleteProblem(Long id);
    void updateProblem(Problem p);
}
