package service;

import domain.Problem;
import repository.ProblemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component("problemService")
public class ProblemServiceImpl implements ProblemService {
    public static final Logger log = LoggerFactory.getLogger(ProblemServiceImpl.class);

    @Autowired
    private ProblemRepository problemRepo;

    @Override
    public void addProblem(Problem p)
    {
        log.trace("addProblem - method entered");
        problemRepo.save(p);
        log.trace("addProblem - method finished");
    }

    @Override
    public Set<Problem> getAll()
    {
        log.trace("getAll - method entered");
        Iterable<Problem> Problem = problemRepo.findAll();
        log.trace("getAll - method finished");
        return StreamSupport.stream(Problem.spliterator(), false).collect(Collectors.toSet());
    }

    @Override
    public Problem getByID(Long id)
    {
        log.trace("getByID - method entered");
        Optional<Problem> problem = problemRepo.findById(id);
        log.trace("getByID - method finished");
        if(problem.isPresent())
            return problem.get();
        else throw new RuntimeException("Couldn't find the problem with the given id");
    }

    @Override
    public Set<Problem> filterProblemsByName(String s)
    {
        log.trace("filterProblemsByName - method entered");
        Iterable<Problem> Problem = problemRepo.findAll();
        Set<Problem> filtered = new HashSet<>();
        Problem.forEach(filtered::add);
        filtered.removeIf(pr -> !pr.getDescr().contains(s));
        log.trace("filterProblemsByName - method finished");
        return filtered;
    }

    @Override
    public void deleteProblem(Long id)
    {
        log.trace("deleteProblem - method entered");
        problemRepo.deleteById(id);
        log.trace("deleteProblem - method finished");

    }

    @Override
    @Transactional
    public void updateProblem(Problem p)
    {
        log.trace("updateProblem - method entered");
        problemRepo.findById(p.getId()).ifPresent(problem -> {
            problem.setDescr(p.getDescr());
            problem.setSerialNumber(p.getSerialNumber());
        });
        log.trace("updateProblem - method finished");
    }
}
