package domain;

import lombok.*;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class Assign extends BaseEntity<Long>{
    private Long studentsid;
    private Long problemsid;
    private int grade;
}
