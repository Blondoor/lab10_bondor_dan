package repository;

import domain.Problem;

public interface ProblemRepository extends Repository<Long, Problem> {
}
