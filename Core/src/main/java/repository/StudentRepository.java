package repository;

import domain.Student;

public interface StudentRepository extends Repository<Long, Student> {
}
