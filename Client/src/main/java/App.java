import dto.StudentDTO;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import ui.Console;

public class App {
        public static final String URL = "http://localhost:8080/api/students";

        public static void main(String[] args)
        {
            AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("config");

            Console console = context.getBean(Console.class);
            console.run();
        }
}
