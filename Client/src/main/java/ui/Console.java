package ui;

import dto.AssignDTO;
import dto.ProblemDTO;
import dto.StudentDTO;
import dto.StudentsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import provider.AssignProvider;
import provider.ProblemProvider;
import provider.StudentProvider;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

@Component
public class Console {
    private static Scanner keyboard = new Scanner(System.in);

    private StudentProvider studentProvider;
    private ProblemProvider problemProvider;
    private AssignProvider assignProvider;

    @Autowired
    public Console(StudentProvider studentProvider, ProblemProvider problemProvider, AssignProvider assignProvider)
    {
        this.studentProvider = studentProvider;
        this.problemProvider = problemProvider;
        this.assignProvider = assignProvider;
    }

    private void addStudent(String[] args){
        StudentDTO student = new StudentDTO(args[3], args[4], Integer.parseInt(args[5]));
        student.setId(Long.parseLong(args[2]));

        studentProvider.save(student);
        System.out.println("Student added successfully");
    }

    private void addProblem(String[] args)
    {
        ProblemDTO problem = new ProblemDTO(args[3], args[4]);
        problem.setId(Long.parseLong(args[2]));

        problemProvider.save(problem);
        System.out.println("Problem added successfully");
    }

    private void addAssignment(String[] args)
    {
        AssignDTO assignment = new AssignDTO(Long.parseLong(args[3]), Long.parseLong(args[4]), Integer.parseInt(args[5]));
        assignment.setId(Long.parseLong(args[2]));

        assignProvider.save(assignment);
        System.out.println("Assignment added successfully");
    }

    private void removeStudent(String[] args)
    {
        this.studentProvider.delete(Long.parseLong(args[2]));
        System.out.println("Student removed successfully");
    }

    private void removeProblem(String[] args)
    {
        this.problemProvider.delete(Long.parseLong(args[2]));
        System.out.println("Problem removed successfully");
    }

    private void removeAssignment(String[] args)
    {
        this.assignProvider.delete(Long.parseLong(args[2]));
        System.out.println("Assignment removed successfully");
    }

    private void updateStudent(String[] args)
    {
        StudentDTO student = new StudentDTO(args[3], args[4], Integer.parseInt(args[5]));
        student.setId(Long.parseLong(args[2]));

        this.studentProvider.update(student);
        System.out.println("Student updated successfully");
    }

    private void updateProblem(String[] args)
    {
        ProblemDTO problem = new ProblemDTO(args[3], args[4]);
        problem.setId(Long.parseLong(args[2]));

        this.problemProvider.update(problem);
        System.out.println("Problem updated successfully");
    }

    private void displayStudents()
    {
        this.studentProvider.getAll().getStudents().forEach(System.out::println);
    }

    private void displayProblems()
    {
        this.problemProvider.getAll().getProblems().forEach(System.out::println);
    }

    private void displayAssignments()
    {
        this.assignProvider.getAll().getAssigns().forEach(System.out::println);
    }

    private void filterStudents(String[] args)
    {
        this.studentProvider.filterStudentsByName(args[2]).getStudents().forEach(System.out::println);
    }

    private void filterProblems(String[] args)
    {
        this.problemProvider.filterProblemsByName(args[2]).getProblems().forEach(System.out::println);
    }

    public void run() {menu();}

    private void menu()
    {
        String cmd;
        System.out.println("--- Lab Problems ---");

        while(true)
        {
            System.out.println(">");
            cmd = keyboard.nextLine();

            String[] cmdArgs = Arrays.stream(cmd.split(" ")).toArray(String[]::new);
            switch(cmdArgs[0])
            {

                case "add":
                    switch (cmdArgs[1]) {
                        case "student":
                            if (cmdArgs.length != 6) {
                                System.out.println("Use: add student [ID] [SerialNumber] [Name] [Group]");
                                continue;
                            } else addStudent(cmdArgs);
                            break;
                        case "problem":
                            if (cmdArgs.length != 5) {
                                System.out.println("Use: add problem [ID] [SerialNumber] [Description]");
                                continue;
                            } else addProblem(cmdArgs);
                            break;
                        case "assignment":
                            if (cmdArgs.length != 6) {
                                System.out.println("Use: add assignment [ID] [Student_ID] [Problem_ID] [Grade]");
                                continue;
                            } else addAssignment(cmdArgs);
                            break;
                        default:
                            break;
                    }
                case "remove":
                    switch (cmdArgs[1])
                    {
                        case "student":
                            if (cmdArgs.length != 3) {
                                System.out.println("Use: remove student [ID]");
                                continue;
                            } else removeStudent(cmdArgs);
                            break;
                        case "problem":
                            if (cmdArgs.length != 3) {
                                System.out.println("Use: remove problem [ID]");
                                continue;
                            } else removeProblem(cmdArgs);
                            break;
                        case "assignment":
                            if (cmdArgs.length != 3) {
                                System.out.println("Use: remove assignment [ID]");
                                continue;
                            } else removeAssignment(cmdArgs);
                            break;
                        default:
                            break;
                    }
                    break;
                case "update":
                    switch (cmdArgs[1])
                    {

                        case "student":
                            if (cmdArgs.length != 6) {
                                System.out.println("Use: add student [ID] [SerialNumber] [Name] [Group]");
                                continue;
                            } else updateStudent(cmdArgs);
                            break;
                        case "problem":
                            if (cmdArgs.length != 5) {
                                System.out.println("Use: add problem [ID] [SerialNumber] [Description]");
                                continue;
                            } else updateProblem(cmdArgs);
                            break;
                        default:
                            break;
                    }
                    break;
                case "display":
                    switch (cmdArgs[1])
                    {
                        case "student":
                            if (cmdArgs.length != 2) {
                                System.out.println("Use: remove student");
                                continue;
                            } else displayStudents();
                            break;
                        case "problem":
                            if (cmdArgs.length != 2) {
                                System.out.println("Use: remove problem");
                                continue;
                            } else displayProblems();
                            break;
                        case "assignment":
                            if (cmdArgs.length != 2) {
                                System.out.println("Use: remove assignment");
                                continue;
                            } else displayAssignments();
                            break;
                        default:
                            break;
                    }
                    break;
                case "filter":
                    switch (cmdArgs[1])
                    {
                        case "student":
                            if (cmdArgs.length != 3) {
                                System.out.println("Use: filter student [Name]");
                                continue;
                            } else filterStudents(cmdArgs);
                            break;
                        case "problem":
                            if (cmdArgs.length != 5) {
                                System.out.println("Use: filter problem [Description]");
                                continue;
                            } else filterProblems(cmdArgs);
                            break;
                        default:
                            break;
                    }
                    break;
                case "report":
                    //getMostAssigned();
                    break;
                case "exit":
                    return;
                default:
                    System.out.println("Invalid command");
            }
        }
    }
}
