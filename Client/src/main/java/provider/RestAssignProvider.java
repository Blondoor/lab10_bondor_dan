package provider;

import dto.AssignDTO;
import dto.AssignsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestAssignProvider implements AssignProvider {
    @Value("http://localhost:8090/api/assignments")
    private String URL;

    private final RestTemplate restTemplate;

    @Autowired
    public RestAssignProvider(RestTemplate restTemplate)
    {
        this.restTemplate = restTemplate;
    }

    @Override
    public void save(AssignDTO dto)
    {
        restTemplate.postForObject(URL, dto, AssignDTO.class);
    }

    @Override
    public void update(AssignDTO dto)
    {
        //restTemplate.put(URL + "/{id}", dto, dto.getId());
    }

    @Override
    public AssignDTO getByID(Long id)
    {
        return restTemplate.getForObject(URL + "/" + id, AssignDTO.class);
    }

    @Override
    public void delete(Long id)
    {
        restTemplate.delete(URL + "/" + id);
    }

    @Override
    public AssignsDTO getAll()
    {
        return restTemplate.getForObject(URL, AssignsDTO.class);
    }
    
}
