package provider;

import dto.StudentDTO;
import dto.StudentsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestStudentProvider implements StudentProvider {
    @Value("http://localhost:8090/api/students")
    private String URL;

    private final RestTemplate restTemplate;

    @Autowired
    public RestStudentProvider(RestTemplate restTemplate)
    {
        this.restTemplate = restTemplate;
    }

    @Override
    public void save(StudentDTO dto)
    {
        restTemplate.postForObject(URL, dto, StudentDTO.class);
    }

    @Override
    public void update(StudentDTO dto)
    {
        restTemplate.put(URL + "/{id}", dto, dto.getId());
    }

    @Override
    public StudentDTO getByID(Long id)
    {
        return restTemplate.getForObject(URL + "/" + id, StudentDTO.class);
    }

    @Override
    public void delete(Long id)
    {
        restTemplate.delete(URL + "/" + id);
    }

    @Override
    public StudentsDTO getAll()
    {
        return restTemplate.getForObject(URL, StudentsDTO.class);
    }

    @Override
    public StudentsDTO filterStudentsByName(String name)
    {
        return restTemplate.getForObject(URL + "/filter/name" + name, StudentsDTO.class);
    }
}
