package provider;

import dto.ProblemDTO;
import dto.ProblemsDTO;

public interface ProblemProvider extends Provider<ProblemDTO> {
    ProblemsDTO getAll();
    ProblemsDTO filterProblemsByName(String descr);
}
