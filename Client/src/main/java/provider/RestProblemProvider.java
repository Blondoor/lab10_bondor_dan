package provider;

import dto.ProblemDTO;
import dto.ProblemsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestProblemProvider implements ProblemProvider {
    @Value("http://localhost:8090/api/problems")
    private String URL;

    private final RestTemplate restTemplate;

    @Autowired
    public RestProblemProvider(RestTemplate restTemplate)
    {
        this.restTemplate = restTemplate;
    }

    @Override
    public void save(ProblemDTO dto)
    {
        restTemplate.postForObject(URL, dto, ProblemDTO.class);
    }

    @Override
    public void update(ProblemDTO dto)
    {
        restTemplate.put(URL + "/{id}", dto, dto.getId());
    }

    @Override
    public ProblemDTO getByID(Long id)
    {
        return restTemplate.getForObject(URL + "/" + id, ProblemDTO.class);
    }

    @Override
    public void delete(Long id)
    {
        restTemplate.delete(URL + "/" + id);
    }

    @Override
    public ProblemsDTO getAll()
    {
        return restTemplate.getForObject(URL, ProblemsDTO.class);
    }

    @Override
    public ProblemsDTO filterProblemsByName(String name)
    {
        return restTemplate.getForObject(URL + "/filter/name" + name, ProblemsDTO.class);
    }
}
