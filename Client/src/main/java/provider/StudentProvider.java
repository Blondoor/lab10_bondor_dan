package provider;

import dto.StudentDTO;
import dto.StudentsDTO;

public interface StudentProvider extends Provider<StudentDTO> {
    StudentsDTO getAll();
    StudentsDTO filterStudentsByName(String name);
}
