package provider;

import dto.AssignDTO;
import dto.AssignsDTO;

public interface AssignProvider extends Provider<AssignDTO> {
    AssignsDTO getAll();
}
