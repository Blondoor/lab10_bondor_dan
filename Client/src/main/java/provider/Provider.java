package provider;

public interface Provider<T> {
    void save(T dto);
    void update(T dto);
    T getByID(Long id);
    void delete(Long id);
}
