package converter;

import domain.Problem;
import dto.ProblemDTO;
import org.springframework.stereotype.Component;

@Component
public class ProblemConverter extends BaseConverter<Problem, ProblemDTO> {
    public Problem convertDTOtoModel(ProblemDTO dto)
    {
        Problem problem = Problem.builder().descr(dto.getDescr())
                                            .serialNumber(dto.getSerialNumber()).build();
        problem.setId(dto.getId());
        return problem;
    }

    public ProblemDTO convertModeltoDTO(Problem problem)
    {
        ProblemDTO dto = ProblemDTO.builder().descr(problem.getDescr())
                                            .serialNumber(problem.getSerialNumber()).build();
        dto.setId(problem.getId());
        return dto;
    }
}
