package converter;

import domain.BaseEntity;
import dto.BaseDTO;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class BaseConverter<Model extends BaseEntity<Long>, Dto extends BaseDTO> implements Converter<Model, Dto>{
    public Set<Long> convertModeltoIDs(Set<Model> models)
    {
        return models.stream().map(BaseEntity::getId).collect(Collectors.toSet());
    }

    public Set<Long> convertDTOstoIDs(Set<Dto> dtos)
    {
        return dtos.stream().map(BaseDTO::getId).collect(Collectors.toSet());
    }

    public Set<Dto> convertModelstoDTOs(Collection<Model> models)
    {
        return models.stream().map(this::convertModeltoDTO).collect(Collectors.toSet());
    }

}
