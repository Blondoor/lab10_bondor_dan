package converter;

import domain.Student;
import dto.StudentDTO;
import org.springframework.stereotype.Component;

@Component
public class StudentConverter extends BaseConverter<Student, StudentDTO> {
    @Override
    public Student convertDTOtoModel(StudentDTO dto)
    {
        Student student = Student.builder().name(dto.getName())
                                            .serialNumber(dto.getSerialNumber())
                                            .sgroup(dto.getSgroup()).build();
        student.setId(dto.getId());
        return student;
    }

    @Override
    public StudentDTO convertModeltoDTO(Student student)
    {
        StudentDTO dto = StudentDTO.builder().name(student.getName())
                                             .serialNumber(student.getSerialNumber())
                                             .sgroup(student.getSgroup()).build();
        dto.setId(student.getId());
        return dto;
    }
}
