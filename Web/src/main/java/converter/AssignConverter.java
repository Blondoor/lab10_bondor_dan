package converter;

import domain.Assign;
import dto.AssignDTO;
import org.springframework.stereotype.Component;

@Component
public class AssignConverter extends BaseConverter<Assign, AssignDTO> {
    public Assign convertDTOtoModel(AssignDTO dto)
    {
        Assign assign = Assign.builder().studentsid(dto.getStudentsid())
                                        .problemsid(dto.getProblemsid())
                                        .grade(dto.getGrade()).build();
        assign.setId(dto.getId());
        return assign;
    }

    public AssignDTO convertModeltoDTO(Assign assign)
    {
        AssignDTO dto = AssignDTO.builder().studentsid(assign.getStudentsid())
                                            .problemsid(assign.getProblemsid())
                                            .grade(assign.getGrade()).build();
        dto.setId(assign.getId());
        return dto;
    }
}
