package converter;

import domain.BaseEntity;
import dto.BaseDTO;

public interface Converter<Model extends BaseEntity<Long>, Dto extends BaseDTO> {
    Model convertDTOtoModel(Dto dto);

    Dto convertModeltoDTO(Model model);
}
