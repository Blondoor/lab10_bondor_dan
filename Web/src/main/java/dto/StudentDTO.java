package dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class StudentDTO extends BaseDTO{
    private String serialNumber;
    private String name;
    private int sgroup;
}
