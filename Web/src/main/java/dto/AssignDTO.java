package dto;


import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
public class AssignDTO extends BaseDTO {
    private Long studentsid;
    private Long problemsid;
    private int grade;
}
