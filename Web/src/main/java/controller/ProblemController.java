package controller;

import converter.ProblemConverter;
import dto.ProblemDTO;
import dto.ProblemsDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.ProblemService;

import java.io.IOException;

@RestController
public class ProblemController {
    public static final Logger log = LoggerFactory.getLogger(ProblemController.class);

    @Autowired
    private ProblemService problemService;

    @Autowired
    private ProblemConverter problemConverter;

    @RequestMapping(value = "/problems", method = RequestMethod.GET)
    ProblemsDTO getProblems()
    {
        log.trace("getProblems - method entered");
        return new ProblemsDTO(problemConverter.convertModelstoDTOs(problemService.getAll()));
    }

    @RequestMapping(value = "/problems", method = RequestMethod.POST)
    void saveProblem(@RequestBody ProblemDTO problemDTO) throws IOException {
        log.trace("saveProblem - method entered");
        problemService.addProblem(problemConverter.convertDTOtoModel(problemDTO));
        log.trace("saveProblem - method finished");
    }

    @RequestMapping(value = "/problems/{id}", method = RequestMethod.PUT)
    void updateProblem(@PathVariable Long id, @RequestBody ProblemDTO problemDTO)
    {
        log.trace("updateProblem - method entered");
        problemDTO.setId(id);
        problemService.updateProblem(problemConverter.convertDTOtoModel(problemDTO));
        log.trace("updateProblem - method finished");
    }

    @RequestMapping(value = "/problems/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteProblem(@PathVariable Long id)
    {
        log.trace("deleteProblem - method entered");
        problemService.deleteProblem(id);
        log.trace("deleteProblem - method finished");

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "problems/filter/{name}", method = RequestMethod.GET)
    ProblemsDTO filterProblemsByName(@PathVariable String name)
    {
        log.trace("filterProblemsByName - method ented");
        return new ProblemsDTO(problemConverter.convertModelstoDTOs(problemService.filterProblemsByName(name)));
    }

    @RequestMapping(value = "/problems/{id}", method = RequestMethod.GET)
    ProblemDTO getByID(@PathVariable Long id)
    {
        log.trace("getByID - method entered");
        return problemConverter.convertModeltoDTO(problemService.getByID(id));
    }
}
