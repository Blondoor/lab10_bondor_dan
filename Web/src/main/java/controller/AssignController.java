package controller;

import converter.AssignConverter;
import dto.AssignDTO;
import dto.AssignsDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.AssignService;

import java.io.IOException;

@RestController
public class AssignController {
    public static final Logger log = LoggerFactory.getLogger(AssignController.class);

    @Autowired
    private AssignService assignService;

    @Autowired
    private AssignConverter assignConverter;

    @RequestMapping(value = "/assignments", method = RequestMethod.GET)
    AssignsDTO getAssignments()
    {
        log.trace("getAssignments - method entered");
        return new AssignsDTO(assignConverter.convertModelstoDTOs(assignService.getAll()));
    }

    @RequestMapping(value = "/assignments", method = RequestMethod.POST)
    void saveAssignment(@RequestBody AssignDTO assignDTO) throws IOException{
        log.trace("saveAssignment - method entered");
        assignService.addAssignment(assignConverter.convertDTOtoModel(assignDTO));
        log.trace("saveAssignment - method finished");
    }

    @RequestMapping(value = "/assignments/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteAssignment(@PathVariable Long id)
    {
        log.trace("deleteAssignment - method entered");
        assignService.deleteAssignment(id);
        log.trace("deleteAssignment - method finished");

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/assignments/{id}", method = RequestMethod.GET)
    AssignDTO getByID(@PathVariable Long id)
    {
        log.trace("getByID - method entered");
        return assignConverter.convertModeltoDTO(assignService.getByID(id));
    }
}
