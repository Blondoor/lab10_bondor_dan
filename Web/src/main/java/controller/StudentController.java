package controller;

import converter.StudentConverter;
import dto.StudentDTO;
import dto.StudentsDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.StudentService;

import java.io.IOException;


@RestController
public class StudentController {
    public static final Logger log = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentConverter studentConverter;

    @RequestMapping(value = "/students", method = RequestMethod.GET)
    StudentsDTO getStudents()
    {
        log.trace("getStudents - method entered");
        return new StudentsDTO(studentConverter.convertModelstoDTOs(studentService.getAll()));
    }

    @RequestMapping(value = "/students", method = RequestMethod.POST)
    void saveStudent(@RequestBody StudentDTO studentDTO) throws IOException{
        log.trace("saveStudent - method entered");
        studentService.addStudent(studentConverter.convertDTOtoModel(studentDTO));
        log.trace("saveStudent - method finished");
    }

    @RequestMapping(value = "/students/{id}", method = RequestMethod.PUT)
    void updateStudent(@PathVariable Long id, @RequestBody StudentDTO studentDTO)
    {
        log.trace("updateStudent - method entered");
        studentDTO.setId(id);
        studentService.updateStudent(studentConverter.convertDTOtoModel(studentDTO));
        log.trace("updateStudent - method finished");
    }

    @RequestMapping(value = "/students/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteStudent(@PathVariable Long id)
    {
        log.trace("deleteStudent - method entered");
        studentService.deleteStudent(id);
        log.trace("deleteStudent - method finished");

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "students/filter/{name}", method = RequestMethod.GET)
    StudentsDTO filterStudentsByName(@PathVariable String name)
    {
        log.trace("filterStudentsByName - method ented");
        return new StudentsDTO(studentConverter.convertModelstoDTOs(studentService.filterStudentsByName(name)));
    }

    @RequestMapping(value = "/students/{id}", method = RequestMethod.GET)
    StudentDTO getByID(@PathVariable Long id)
    {
        log.trace("getByID - method entered");
        return studentConverter.convertModeltoDTO(studentService.getByID(id));
    }
}
